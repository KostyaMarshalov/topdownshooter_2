// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS2GameMode.h"
#include "TPS2PlayerController.h"
#include "TPS2Character.h"
#include "UObject/ConstructorHelpers.h"

ATPS2GameMode::ATPS2GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS2PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}