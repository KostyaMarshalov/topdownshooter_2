// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPS2_TPS2GameMode_generated_h
#error "TPS2GameMode.generated.h already included, missing '#pragma once' in TPS2GameMode.h"
#endif
#define TPS2_TPS2GameMode_generated_h

#define TPS2_Source_TPS2_TPS2GameMode_h_12_SPARSE_DATA
#define TPS2_Source_TPS2_TPS2GameMode_h_12_RPC_WRAPPERS
#define TPS2_Source_TPS2_TPS2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TPS2_Source_TPS2_TPS2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATPS2GameMode(); \
	friend struct Z_Construct_UClass_ATPS2GameMode_Statics; \
public: \
	DECLARE_CLASS(ATPS2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS2"), TPS2_API) \
	DECLARE_SERIALIZER(ATPS2GameMode)


#define TPS2_Source_TPS2_TPS2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATPS2GameMode(); \
	friend struct Z_Construct_UClass_ATPS2GameMode_Statics; \
public: \
	DECLARE_CLASS(ATPS2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS2"), TPS2_API) \
	DECLARE_SERIALIZER(ATPS2GameMode)


#define TPS2_Source_TPS2_TPS2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TPS2_API ATPS2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATPS2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TPS2_API, ATPS2GameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPS2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TPS2_API ATPS2GameMode(ATPS2GameMode&&); \
	TPS2_API ATPS2GameMode(const ATPS2GameMode&); \
public:


#define TPS2_Source_TPS2_TPS2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TPS2_API ATPS2GameMode(ATPS2GameMode&&); \
	TPS2_API ATPS2GameMode(const ATPS2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TPS2_API, ATPS2GameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPS2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATPS2GameMode)


#define TPS2_Source_TPS2_TPS2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define TPS2_Source_TPS2_TPS2GameMode_h_9_PROLOG
#define TPS2_Source_TPS2_TPS2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS2_Source_TPS2_TPS2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TPS2_Source_TPS2_TPS2GameMode_h_12_SPARSE_DATA \
	TPS2_Source_TPS2_TPS2GameMode_h_12_RPC_WRAPPERS \
	TPS2_Source_TPS2_TPS2GameMode_h_12_INCLASS \
	TPS2_Source_TPS2_TPS2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TPS2_Source_TPS2_TPS2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS2_Source_TPS2_TPS2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TPS2_Source_TPS2_TPS2GameMode_h_12_SPARSE_DATA \
	TPS2_Source_TPS2_TPS2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TPS2_Source_TPS2_TPS2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	TPS2_Source_TPS2_TPS2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPS2_API UClass* StaticClass<class ATPS2GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TPS2_Source_TPS2_TPS2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
