// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPS2_TPS2PlayerController_generated_h
#error "TPS2PlayerController.generated.h already included, missing '#pragma once' in TPS2PlayerController.h"
#endif
#define TPS2_TPS2PlayerController_generated_h

#define TPS2_Source_TPS2_TPS2PlayerController_h_12_SPARSE_DATA
#define TPS2_Source_TPS2_TPS2PlayerController_h_12_RPC_WRAPPERS
#define TPS2_Source_TPS2_TPS2PlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TPS2_Source_TPS2_TPS2PlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATPS2PlayerController(); \
	friend struct Z_Construct_UClass_ATPS2PlayerController_Statics; \
public: \
	DECLARE_CLASS(ATPS2PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS2"), NO_API) \
	DECLARE_SERIALIZER(ATPS2PlayerController)


#define TPS2_Source_TPS2_TPS2PlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATPS2PlayerController(); \
	friend struct Z_Construct_UClass_ATPS2PlayerController_Statics; \
public: \
	DECLARE_CLASS(ATPS2PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS2"), NO_API) \
	DECLARE_SERIALIZER(ATPS2PlayerController)


#define TPS2_Source_TPS2_TPS2PlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATPS2PlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATPS2PlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPS2PlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPS2PlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPS2PlayerController(ATPS2PlayerController&&); \
	NO_API ATPS2PlayerController(const ATPS2PlayerController&); \
public:


#define TPS2_Source_TPS2_TPS2PlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPS2PlayerController(ATPS2PlayerController&&); \
	NO_API ATPS2PlayerController(const ATPS2PlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPS2PlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPS2PlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATPS2PlayerController)


#define TPS2_Source_TPS2_TPS2PlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define TPS2_Source_TPS2_TPS2PlayerController_h_9_PROLOG
#define TPS2_Source_TPS2_TPS2PlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_SPARSE_DATA \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_RPC_WRAPPERS \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_INCLASS \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TPS2_Source_TPS2_TPS2PlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_SPARSE_DATA \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_INCLASS_NO_PURE_DECLS \
	TPS2_Source_TPS2_TPS2PlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPS2_API UClass* StaticClass<class ATPS2PlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TPS2_Source_TPS2_TPS2PlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
