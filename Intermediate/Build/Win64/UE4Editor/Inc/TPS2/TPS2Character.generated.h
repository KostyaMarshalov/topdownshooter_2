// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TPS2_TPS2Character_generated_h
#error "TPS2Character.generated.h already included, missing '#pragma once' in TPS2Character.h"
#endif
#define TPS2_TPS2Character_generated_h

#define TPS2_Source_TPS2_TPS2Character_h_13_SPARSE_DATA
#define TPS2_Source_TPS2_TPS2Character_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define TPS2_Source_TPS2_TPS2Character_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execInputAxisY);


#define TPS2_Source_TPS2_TPS2Character_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATPS2Character(); \
	friend struct Z_Construct_UClass_ATPS2Character_Statics; \
public: \
	DECLARE_CLASS(ATPS2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS2"), NO_API) \
	DECLARE_SERIALIZER(ATPS2Character)


#define TPS2_Source_TPS2_TPS2Character_h_13_INCLASS \
private: \
	static void StaticRegisterNativesATPS2Character(); \
	friend struct Z_Construct_UClass_ATPS2Character_Statics; \
public: \
	DECLARE_CLASS(ATPS2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TPS2"), NO_API) \
	DECLARE_SERIALIZER(ATPS2Character)


#define TPS2_Source_TPS2_TPS2Character_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATPS2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATPS2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPS2Character); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPS2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPS2Character(ATPS2Character&&); \
	NO_API ATPS2Character(const ATPS2Character&); \
public:


#define TPS2_Source_TPS2_TPS2Character_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPS2Character(ATPS2Character&&); \
	NO_API ATPS2Character(const ATPS2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPS2Character); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPS2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATPS2Character)


#define TPS2_Source_TPS2_TPS2Character_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ATPS2Character, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATPS2Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(ATPS2Character, CursorToWorld); }


#define TPS2_Source_TPS2_TPS2Character_h_10_PROLOG
#define TPS2_Source_TPS2_TPS2Character_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS2_Source_TPS2_TPS2Character_h_13_PRIVATE_PROPERTY_OFFSET \
	TPS2_Source_TPS2_TPS2Character_h_13_SPARSE_DATA \
	TPS2_Source_TPS2_TPS2Character_h_13_RPC_WRAPPERS \
	TPS2_Source_TPS2_TPS2Character_h_13_INCLASS \
	TPS2_Source_TPS2_TPS2Character_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TPS2_Source_TPS2_TPS2Character_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TPS2_Source_TPS2_TPS2Character_h_13_PRIVATE_PROPERTY_OFFSET \
	TPS2_Source_TPS2_TPS2Character_h_13_SPARSE_DATA \
	TPS2_Source_TPS2_TPS2Character_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	TPS2_Source_TPS2_TPS2Character_h_13_INCLASS_NO_PURE_DECLS \
	TPS2_Source_TPS2_TPS2Character_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TPS2_API UClass* StaticClass<class ATPS2Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TPS2_Source_TPS2_TPS2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
